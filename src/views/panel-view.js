var React = require('react/addons');
var mediator = require('../lib/mediator');
var classNames = require('classnames');
var ReactCSSTransitionGroup = React.addons.CSSTransitionGroup;

module.exports = React.createClass({

  getClasses: function(){
    var count = this.props.panelCount;
    var classObj = {};
    
    if (this.props.stacked){
      // Stacked panels
      classObj['nested-child'] = true;
      classObj['col-sm-12'] = true;
      classObj['stacked'] = true;
    } else {
      // Single Panel left side
      classObj['col-sm-8'] = true;
    }

    classObj['col-xs-12'] = true;

    classObj['panel'] = true;
    classObj['panelNum' + count ] = true;

    return classObj;
  },

  render: function(){
    var componentClasses = classNames(this.getClasses());

    return (
      <ReactCSSTransitionGroup transitionName="fade" transitionLeave={false}>
        <div className={componentClasses} key={this.props.panelCount}>
          <div className="inner-panel">
            <div className="panel-count circle">{this.props.panelCount}</div>
            { this.props.children }
          </div>
        </div>
      </ReactCSSTransitionGroup>
    )
  }
}); 