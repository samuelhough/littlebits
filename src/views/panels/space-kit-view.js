var React = require('react/addons')
var classNames = require('classnames');
var mediator = require('../../lib/mediator');

module.exports = React.createClass({
  rearrange: function(){
    mediator.emit('change:places', this);
  },
  render: function(){
    return (
      <article onClick={this.clickHandler} className="space-kit">
        <h2><strong>Introducing:</strong></h2>
        <h1><strong>The Space Kit</strong></h1>
        <div>In Partnership with Nasa</div>
        <button onClick={this.rearrange}><strong>Rearrange</strong></button>
      </article>
    );
  }
}); 