var React = require('react/addons');

module.exports = React.createClass({

  render: function(){
    return (
      <article onClick={this.clickHandler} className="shipping">
        <span> Enjoy </span>
        <h1><strong>Free Shipping and Extra Joy</strong></h1>
        <span className="small-txt">on orders $60+ <strong>limited time offer</strong></span>
      </article>
    );
  }
}); 