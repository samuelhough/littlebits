var React = require('react/addons');

module.exports = React.createClass({

  render: function(){
    return (
      <article onClick={this.clickHandler} className="new-bits">
        
        <h1><strong>New to LittleBits?</strong></h1>
        <h3>Let us help get you started</h3>
        <button ><strong>Lets get started</strong></button>
      </article>
    );
  }
}); 