var React = require('react/addons');
var Router = require('react-router');
var Link = Router.Link;

module.exports = React.createClass({
  render: function(){
    return (
      <nav className="navbar">
        <div className="navbar-leftside">
          <div className="user-profile">
            <Link to="/settings">
              <i className="fa fa-user"></i>
            </Link>
          </div>
          <Link to="/make" className="make-btn bttn">make</Link>
        </div>
          <div className="bits-logo-container ">
            <Link to="/">
              <img src="/images/littlebits-logo.svg" alt="Littlebits Logo" className="bits-logo" />
            </Link>
          </div>
        <div className="navbar-rightside">
          <Link to="/shop" className="shop-btn bttn">shop</Link>
          <div className="shopping-cart">
            <Link to="/cart">
              <i className="fa fa-shopping-cart"></i>
            </Link>
          </div>
        </div>
      </nav>
    )
  }
}); 