var React = window.React = require('react/addons');
var mediator = require('../lib/mediator');
var HeaderV = require('./header-view');

var PanelV = require('./panel-view');
var SpaceKitV = require('./panels/space-kit-view');
var NewBitsV = require('./panels/new-bits-view');
var ShippingV = require('./panels/shipping-view');

module.exports = React.createClass({
  getInitialState: function(){
    return { 
      subviews: [(<SpaceKitV />),(<NewBitsV />),(<ShippingV />)],
      order: [1,2,3]
    };
  },
  componentDidMount: function(){
    mediator.on('change:places', this.swapPanels, this);
  },
  swapPanels: function(){
    var views = this.state.subviews;
    var order = this.state.order;
    var lastV = views.pop();
    var lastOrder = order.pop();

    views.unshift(lastV);
    order.unshift(lastOrder);

    this.setState({
      'subviews': views,
      'order': order
    });
  },
  render: function(){
    return (
      <div className="littlebits-app">
        <HeaderV />
        <div className="app-content">
          <div>
            <PanelV panelCount={this.state.order[0]} stacked={false} >
                {this.state.subviews[0]}
            </PanelV>
            <div className="col-xs-12 col-sm-4 nested-child">
              <PanelV panelCount={this.state.order[1]} stacked={true} >
                  {this.state.subviews[1]}
              </PanelV>
              <PanelV panelCount={this.state.order[2]} stacked={true} >
                  {this.state.subviews[2]}
              </PanelV>
            </div>
          </div>
        </div>
      </div>
    )
  },
  componentWillUnmount: function(){
    mediator.off('change:places', this.adjustPlacements, this);
  }
}); 